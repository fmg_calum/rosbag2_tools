#!/usr/bin/python3

# pylint: disable=import-error
import os
import argparse
import numpy as np

from beautifultable import BeautifulTable

from plotly import __version__
from plotly.offline import download_plotlyjs
from plotly.graph_objs import *

import plotly.graph_objs as go


from rosidl_runtime_py.utilities import get_message
from rosidl_runtime_py.utilities import get_namespaced_type
from rosidl_runtime_py.import_message import import_message_from_namespaced_type
from rosidl_runtime_py.convert import get_message_slot_types
from rosidl_parser.definition import NamespacedType

import rosbag2_py._rosbag2_py as rosbag2_py
from rclpy.serialization import deserialize_message

class PlotObject:
    """A simple class holding all info required to graph a topics field against another field.
    
    Args:
        y_topic_name (string): A string defining the topic name that the yaxis refers to.
        y_field_name (string): A string defining the field name within the y_topic_name topic that is graphed on the yaxis.
        x_topic_name (string): A string defining the topic name that the xaxis refers to.
                             The default index means that a simple incrementing integer is used to graphed the yaxis values against.
        x_field_name (string): A string defining the field name within the x_topic_name topic that is graphed on the xaxis.
                             The defualt value None means that a simple incrementing integer is used to graphed the yaxis values against.

    Returns:
       BeautifulTable: A table preformatted with the given headings."""
    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-few-public-methods

    # The defualt index and None value for the x topic name and field means to just plot against the index number of each entry.

    def __init__(self, y_topic_name, y_field_name, x_topic_name="index", x_field_name=None):
        self.plot_name = str(y_topic_name) + '.' + str(y_field_name)
        self.x_topic_name = x_topic_name
        self.y_topic_name = y_topic_name
        self.x_field_name = x_field_name
        self.y_field_name = y_field_name
        self.x_array = []
        self.y_array = []
        self.x_known_field = False
        self.y_known_field = False


def parse_arguments():
    """Parse and validate the command line arguments.

    Returns:
        dict: a collection of argument names and their parsed value.
    """
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(title='subcommands', dest='subcommand', help='Additional help')

    parser_list = subparsers.add_parser('list', help='List topic and message types')
    parser_list.add_argument('type', choices=['topics', 'messages'], help='Select topics or message types')
    parser_list.add_argument('-n', '--names', dest='names', help='Specify the names of the topics or messages to list.')
    parser_list.add_argument('rosbag', help='The file path of the bag to be interregated')

    parser_extract = subparsers.add_parser('extract', help='Extract and recreate the .msg files used in the bag. The files will be ouput to the directory containing the bag file.')
    parser_extract.add_argument('type', choices=['topics', 'messages'], help='Select topics or message types to extract')
    parser_extract.add_argument('-n', '--names', dest='names', help='Specify the names of the topics or messages to extract.')
    parser_extract.add_argument('rosbag', help='The file path of the bag to be interregated')
    parser_extract.add_argument('-o', '--outputDir', dest='output_dir', help='The path of the directory to output the .mgs file to - defaults to directory containing the bag')

    parser_plot = subparsers.add_parser('plot', help='Plot the selected topic message fields.')
    parser_plot.add_argument('-x', '--xvalues', dest='xvalues', required=False, help='Specify the topic name and data field to use in the xaxis')
    parser_plot.add_argument('-y', '--yvalues', dest='yvalues', required=True, help='Specify the topic name and data field to use in the xaxis')
    parser_plot.add_argument('rosbag', help='The file path of the bag to be interregated')

    return parser.parse_args()

def setup_table(headings):
    """Create a BeautifulTable with predefined formatting.

    Args:
        headings (list(string)): A list a string, each one is a heading for the table. Order is maintained.

    Returns:
       BeautifulTable: A table preformatted with the given headings.
    """
    try:
        max_table_width = int(subprocess.check_output(['stty', 'size']).split()[1])  # console width
    except:                                                             # pylint: disable=bare-except
        max_table_width = 180 # fallback

    table = BeautifulTable(max_width=max_table_width)                   # pylint: disable=maybe-no-member
    table.set_style(BeautifulTable.STYLE_SEPARATED)                     # pylint: disable=maybe-no-member
    table.column_headers = headings
    for header in headings:
        table.column_alignments[header] = BeautifulTable.ALIGN_LEFT     # pylint: disable=maybe-no-member
    return table

def print_topics(topics_type, message_field_names, names=None):
    """Print a table containing a list of Topics and their corresponding MessageTypes.

    Args:
        topics_type (dict(string, string)): Topics to associated MessageType. These are formatted and printed to the console.
        message_field_names(dist(string, string)): A dict of unique MessageTypes with the field names.
        names (list(string)): A list of topic names to print out the message type and field values for.
    """
    if names is None:
        table = setup_table(["Topic", "Message Type"])
        for topic, message_type in topics_type.items():
            table.append_row([topic, message_type])
    else:
        table = setup_table(["Topic", "Message Type", "Fields"])
        for topic in names:
            if topic in topics_type:
                message_type = topics_type[topic]
                field_names = message_field_names[message_type]
                table.append_row([topic, message_type, field_names])
    table.sort("Topic")
    print(table)

def print_all_message_types(message_definitions, message_field_names):
    """Print a table containing a list of MessageTypes to the console.

    Args:
        message_definitions (list(string)): These are formatted and printed to the console.
        message_field_names (dict(string, string)): A dict of unique MessageTypes with the field names.
    """
    # pylint: disable=unused-variable
    table = setup_table(["Message Type", "Message Definition"])
    for message_type, message_def in message_definitions.items():
        table.append_row([message_type, message_field_names[message_type]])
    table.sort("Message Type")
    print(table)


def extract_info_from_bag(bag_path):
    """Given a rosbag, extracts Topics, MessageTypes, and MessageDefinitions.

    Args:
        bag_path (string): The file path of the rosbag.

    Returns:
        dict(string, string): topics_type to associated MessageType.
        dict(string, string): MessageTypes to associated MessageDefinition. MessageDefinitions are multiline strings.
        Sequential reader: A rosbag2_py. Sequential reader that can read rosbag2 logs.
    """
    # pylint: disable=unused-variable
    topics_type = {}                                 # A dict of unique Topics to associated MessageType
    message_definitions = {}                    # A dict of unique MessageTypes to associated MessageDefinition
    message_field_names = {}  # A dict of unique MessageTypes with the field names.


    # Process the bag
    print("bag_path = %s" % bag_path)
    try:
        reader = rosbag2_py.SequentialReader()
        reader.open(bag_path)
    except IOError as ex:
        print("I/O error: {0}".format(ex))
        exit(1)

    topics_type = reader.get_all_topics_and_types()
    for topic, message_type in topics_type.items():
       
        ns_typ = get_namespaced_type(message_type)
        field_elem_type = import_message_from_namespaced_type(ns_typ)
        submsg = field_elem_type()
        # Message definitions just gets filled in with an empty message of that particular type.
        # There is no simple way of getting the message definitions.
        message_definitions[message_type] = submsg

        # Get the message field defintions. There is no easy clean way to do this currently.
        slot_types = get_message_slot_types(submsg) 
        for field_name in slot_types:
            #import ipdb; ipdb.set_trace()
            if isinstance(slot_types[field_name], NamespacedType):
                field_type = import_message_from_namespaced_type(slot_types[field_name])
            else:
                #import ipdb; ipdb.set_trace()
                try:
                    field_type = slot_types[field_name].typename
                except AttributeError:
                    # Some types don't have a typename e.g Apex boundedString.
                    field_type = slot_types[field_name].__class__.__name__

            if message_type in message_field_names:
                message_field_names[message_type] = message_field_names[message_type] + ", " + str(field_type) + ": " + field_name
            else:
                message_field_names[message_type] = str(field_type) + ": " + field_name

    return topics_type, message_definitions, message_field_names, reader

def _write_topic_types_to_file(topics_type, output_dir):
    """Write a file containing the Topics and their associated MessageTypes.

    Args:
        output_dir (string): Path of the directory to output files to.
        topics_type (dict(string, string)): Topics to associated MessageType.
    """
    topic_file = open(output_dir + "/topics_types.txt", "w+")
    topic_file.write("Topic : MessageType\r\n")
    for topic_name in topics_type:
        topic_file.write(topic_name + " : " + topics_type[topic_name] + "\r\n")
    topic_file.close()

def _write_message_definition_file(message_type, message_contents, output_dir):
    """Write MessageDefinition to .msg file.

    Args:
        message_type (string): The MessageType. This may contain '/' for hirachial types.
        message_contents (string): The MessageDefinition for the MessageType.
        output_dir (string): The name of the directory to store the message definitions in.
    """
    message_definition_file_path = output_dir + "/" + message_type + ".msg"
    message_definition_dir = os.path.dirname(os.path.realpath(message_definition_file_path))
    if not os.path.exists(message_definition_dir):
        os.makedirs(message_definition_dir)
    message_definition_file = open(message_definition_file_path, "w+")
    message_definition_file.write(str(message_contents))
    message_definition_file.close()

def write_messages_definitions_to_file(output_dir, topics_type, message_definitions):
    """Write a topic list and message definitions out to files.

    A topics file will be produced that contains each topic and its corresponding MessageType.

    Each MessageDefinition will also be written out to file. These .msg files are the same as the originals.

    Args:
        output_dir (string): Path of the directory to output files to.
        topics_type (dict(string, string)): Topics to associated MessageType.
        message_definitions (dict(string, string)): MessageTypes to associated MessageDefinition.
    """

    print("Writing message definitions")

    # Test to see if the directory already exists as we don't want to overwrite it
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
        print("Created output directory : " + output_dir)
    else:
        print("Output directory already exists : " + output_dir)

    # Write out topic types list
    _write_topic_types_to_file(topics_type, output_dir)

    # Write out the message definitions
    for message_type, message_contents in message_definitions.items():
        _write_message_definition_file(message_type, message_contents, output_dir)

    print("Done")

def write_topics_to_file(output_dir, topics_type, reader, select_topics):
    """Write all the given topics and their associated data to a file.

    A topics file will be produced that contains each topic and its value.

    Args:
        output_dir (string): Path of the directory to output files to.
        topics_type (dict(string, string)): Topics to associated MessageType.
        reader (rosbag2_py.sequential.reader): A rosbag2_py Sequential reader object. This reads the rosbag2 and deserialises the data.
        select_topics (list(string)): A list of the topic names to write to the file.
    """

    print("Writing topic values")

    # Test to see if the directory already exists as we don't want to overwrite it
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
        print("Created output directory : " + output_dir)
    else:
        print("Output directory already exists : " + output_dir)

    # Write out topic list
    topic_file = open(output_dir + "/topics.txt", "w+")
    topic_file.write("Topic : Data\r\n")
    i = 0 
    while reader.has_next():
        i += 1
        topic, data = reader.read_next()

        if (topic in select_topics):
            msg_type = get_message(topics_type[topic])
            msg = deserialize_message(data, msg_type)
            topic_file.write(str(topic) + " : " + str(msg) + "\r\n")

    topic_file.close()

    print("Done")

def _check_for_known_field(values_fields):
    """Check if the given field is of a known type.

    A known topic field may be treated differently when processing the data.
    E.g a time stamp field may be converted into a format that can be plotted against (total seconds)
    instead of being left as nanosecs and sec fields.

    Args:
        values_fields (string): A string defining a field within a topic.

    Returns:
       Boolean: Specifies if this field is a known type or not.
    """
    if values_fields is not None:
        if (values_fields[-1] == "stamp"):
            return True

    return False

def _convert_known_field(data, xvalues_field):
    """convert the input data into a more useful format.

    Args:
        data (list(array(values))): list of x values for each plot.
        xvalues_field (string): A string defining a field to convert.

    Returns:
        converted_value: The value of the converted field.
    """
    last_field = xvalues_field[-1]

    if (last_field == "stamp"):
        return float(data.sec+float(data.nanosec/1e9))
    return data

def _plot_xy(title, xvalues, yvalues, name):
    """plot the x values against y values and give the plot the given legend name.

    Args:
        xvalues (list(array(values))): list of x values for each plot
        yvalues (list(array(values))): list of y values for each plot
        title (string): Title of the graph
        name (list(string)): Name of each line plot
    """

    # Plot each of the variables in the var_names list
    data = [
        go.Scatter(
            x=np.array(xvalues[idx]).flatten(), 
            y=np.array(yvalues[idx]).flatten(),
            name=str(name[idx]),
            yaxis='y' if idx <= 0 else 'y' + str(idx+1),
            xaxis='x' if idx <= 0 else 'x' + str(idx+1)
            #mode = 'markers
        ) for idx, yarray in enumerate(yvalues)
    ]


    # Set the layout
    layout = Layout(title=title)

    #Set the number of xaxis, also offset them slightly so they aren't all ontop of one another.
    xaxis = [go.layout.XAxis(title='X axis1')]
    for idx in range(len(xvalues)-1):
        xaxis.append(go.layout.XAxis(title="X axis" + str(idx+2),
                                     anchor="free",
                                     overlaying="x",
                                     side="bottom",
                                     position=(idx+1)*0.05))
    for ind, _ in enumerate(xaxis):
        layout['xaxis'+str(ind+1)] = xaxis[ind]

    #Set the number of yaxis, also offset them slightly so they aren't all ontop of one another.
    yaxis = [go.layout.YAxis(title="Y axis1")]
    for idx in range(len(yvalues)-1):
        yaxis.append(go.layout.YAxis(title="Y axis" + str(idx+2),
                                     anchor="free",
                                     overlaying="y",
                                     side="left" if idx <= 1 else "right",
                                     position=(idx+1)*0.05))

    for ind, _ in enumerate(yaxis):
        layout['yaxis'+str(ind+1)] = yaxis[ind]


    fig_widget = go.Figure(data, layout=layout)
    fig_widget.show()

def plot_topics(topics_type, reader, title, xtopicfieldstr, ytopicfieldstr):
    """Plot the given topics.

    Args:
        topics_type (dict(string, string)): Topics to associated MessageType.
        reader (rosbag2_py.Sequential.reader): A rosbag2_py Sequential reader object. This reads the rosbag2 and deserialises the data.
        title (string): The title of the graph.
        xtopicfieldstr (string): e.g /current_pose.header.stamp        If this is empty then just use the index of each y value.
        ytopicfieldstr (string): e.g /current_pose.pose.position.x     This can be a comma seperated list for mutpile plots. 
    """
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-branches
    # pylint: disable=len-as-condition
    # Get the topic name from the xvalues input.
    # e.g /current_pose.header.stamp.sec get the /current_pose
    plot_obj_list = []

    if (ytopicfieldstr is not None):    
        # We may be plotting multiple topic values if the input is a list of topic names and fields
        ytopicfieldlist = [it for it in ytopicfieldstr.split(',')]
        for ytopicfield in ytopicfieldlist:
            yvalues_topic = ytopicfield.split('.')[0]
            yvalues_field = ytopicfield.split('.')[1:]
            new_plot_obj = PlotObject(yvalues_topic, yvalues_field)
            # Check if the field given last subfield is a known field e.g stamp for timestamps etc..
            new_plot_obj.y_known_field = _check_for_known_field(yvalues_field)
            plot_obj_list.append(new_plot_obj)

    if (xtopicfieldstr is not None):
        # We may be plotting multiple topic values if the input is a list of topic names and fields
        xtopicfieldlist = [it for it in xtopicfieldstr.split(',')]
        for idx, xtopicfield in enumerate(xtopicfieldlist):
            xvalues_topic = xtopicfield.split('.')[0]
            xvalues_field = xtopicfield.split('.')[1:]
            plot_obj_list[idx].x_topic_name = xvalues_topic
            plot_obj_list[idx].x_field_name = xvalues_field
            # Check if the field given last subfield is a known field e.g stamp for timestamps etc..
            plot_obj_list[idx].x_known_field = _check_for_known_field(xvalues_field)
    else:
        xvalues_topic = "index"
        xvalues_field = None

    print("Plotting topic(s)")
    i = 0 

    # Get a list of all the x and y topic names that will be plotted.
    x_topic_names = [tn.x_topic_name for tn in plot_obj_list]
    y_topic_names = [tn.y_topic_name for tn in plot_obj_list]

    # Go through each message in the rosbag2. 
    # Check if it is one we want to plot and then deserialize it if so.
    while reader.has_next():
        i += 1
        topic, data = reader.read_next()

        #import ipdb; ipdb.set_trace()
        if (topic in x_topic_names) or (topic in y_topic_names):
            msg_type = get_message(topics_type[topic])
            msg = deserialize_message(data, msg_type)
            # print("TOPIC = \n", str(topic))
            # print("DATA = \n", str(data))
            for idx, plt_ob in enumerate(plot_obj_list):
                # Check the x topic names of the plot objects
                if plt_ob.x_topic_name == topic:
                    # Get the xvalue
                    result = _get_sub_attr(msg, plt_ob.x_field_name)
                    if (plt_ob.x_known_field):
                        result = _convert_known_field(result, plt_ob.x_field_name)
                    plt_ob.x_array.append(result)
                # Check the y topic names of the plot objects
                if plt_ob.y_topic_name == topic:
                    # Get the yvalue
                    result = _get_sub_attr(msg, plt_ob.y_field_name)
                    #import ipdb; ipdb.set_trace()
                    if (plt_ob.y_known_field):
                        result = _convert_known_field(result, plt_ob.y_field_name)
                    plt_ob.y_array.append(result)

    # Make sure any unfilled x_values are initialized to an incrementing array.
    for plt_ob in plot_obj_list:
        if (len(plt_ob.x_array) == 0):
            plt_ob.x_array = np.arange(len(plt_ob.y_array))
    # Get a list of the x and y value arrays.
    list_x_values = [tn.x_array for tn in plot_obj_list]
    list_y_values = [tn.y_array for tn in plot_obj_list]
    list_plot_names = [tn.plot_name for tn in plot_obj_list]

    # Now for call the plot function
    _plot_xy(title, list_x_values, list_y_values, list_plot_names)

    print("Done")

def _get_sub_attr(object_in, sub_attr_list):
    """Get the sub attribute from the message object and return it.

    Args:
        object_in (object): A deserialised message object.
        sub_attr_list (string): The name of the sub attribute to get from the message object.

    Returns:
        attribute_object: The attribute from the message object.
    """
    # Get the sub atrribute (the sub slot)
    # This is done because getattr func doesn't have a niceway for calling sub-attributes.
    if (len(sub_attr_list) == 1):
        return getattr(object_in, sub_attr_list[0])  
    else:
        # Remove the last element in the list and recursivly call this func
        sub_attr_deepest = sub_attr_list[-1]
        sub_attr_list = sub_attr_list[0:-1]
        attr_res = _get_sub_attr(object_in, sub_attr_list)
        return getattr(attr_res, sub_attr_deepest)


def main():
    """Main execution function. Performs operation defined by the command line arguments.

    example usage:
    python3 rosbag2tools.py list topics ../src/rosbag2_2020_01_22-11_53_10/rosbag2_2020_01_22-11_53_10_0.db3
    """
    # pylint: disable=too-many-branches
    # Parse arguments
    args = parse_arguments()

    # Variables
    bag_path = args.rosbag                      # The path to the rosbag
    topics_type = {}                                 # A map of unique Topic Names to its MessageType
    message_definitions = {}                    # A map of unique MessageType to its MessageDefinition
    message_field_names = {}  # A dict of unique MessageTypes with the field names.
    reader = None                               # A rosbag2_py sequential reader.

    topics_type, message_definitions, message_field_names, reader = extract_info_from_bag(bag_path)

    # set the selected topics to act on as intially all of them. We set this later on to just the user defined topics.
    select_topics = list(topics_type.keys())

    ##################### list command ####################################
    if (args.subcommand == 'list') and (args.type == 'topics'):
        if args.names:
            topic_names = [it for it in args.names.split(',')]
            print_topics(topics_type, message_field_names, topic_names)
        else:
            print_topics(topics_type, message_field_names)
    elif (args.subcommand == 'list') and (args.type == 'messages'):
        print_all_message_types(message_definitions, message_field_names)

    ##################### extract command ###########################
    # usage:
    #    python3 rosbag2tools.py extract topics -n "/current_pose,/current_velocity" ../src/rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3

    elif (args.subcommand == 'extract') and (args.type == 'messages'):
        output_dir = args.output_dir
        if not output_dir:
            bag_file = os.path.basename(bag_path)
            bag_name = os.path.splitext(bag_file)[0]
            bag_dir = os.path.dirname(os.path.realpath(bag_path))
            output_dir = bag_dir + "/" + bag_name

        write_messages_definitions_to_file(output_dir, topics_type, message_definitions)

    elif (args.subcommand == 'extract') and (args.type == 'topics'):
        if args.names:
            # A list of topic names have been given, set the selected topics to this arg list.
            select_topics = [it for it in args.names.split(',')]
            check = all(item in list(topics_type.keys()) for item in select_topics)
            if not check:
                print("Not all topics names found in bag file")
                print("selected topics = ", select_topics)
                print("list of topics found) = ", list(topics_type.keys()))
                exit()

        print("Selected topics = ", select_topics)
        output_dir = args.output_dir
        if not output_dir:
            bag_file = os.path.basename(bag_path)
            bag_name = os.path.splitext(bag_file)[0]
            bag_dir = os.path.dirname(os.path.realpath(bag_path))
            output_dir = bag_dir + "/" + bag_name
        write_topics_to_file(output_dir, topics_type, reader, select_topics)

    ##################### Plot command ###########################
    # usage:
    #    python3 rosbag2tools.py plot -y "/current_pose.pose.position.x" ../src/rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3
    #    python3 rosbag2tools.py plot -x /current_pose.pose.position.x,index.None,index.None,/current_velocity.header.stamp
    #                                 -y /current_pose.pose.position.y,/pacmod/as_rx/steer_cmd.command,/pacmod/as_rx/accel_cmd.command,/current_velocity.twist.linear.x 
    #                                 ../src/rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3

    elif (args.subcommand == 'plot'):
        print("xvalues = ", args.xvalues)
        print("yvalues = ", args.yvalues)
        plot_topics(topics_type, reader, bag_path, args.xvalues, args.yvalues)

    else:
        args.print_help()


if __name__ == "__main__":
    main()
