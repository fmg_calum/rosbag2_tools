# rosbag2_tools

A collection of useful tools to decode and plot data from roabg2 logs. This repo requires ros2 Foxy Fitzroy or at least a version of rclpy newer then #677de6f2. Download an ADE docker image with the required prerequisites, see this repos container registry.  

# To use this tool enter the docker container.

It's easiest to use the ade-cli tool https://gitlab.com/ApexAI/ade-cli.  

Run `ade start` then `ade enter` you should now have a bash shell in this docker container.    

Copy the rosbag2 logs you want to decode into the mounted home dir (where the .adehome file is).  

You can now run commands from the rosabg2 tool. This script and all the requirements are already setup in the container.  
You should be able to run   
`rosbag2tools.py --h` for help.  

**Custom Message Types**  
In order to decode custom messages you will need to build and source them before running the rosbag2tool.py function.  

## list all topics  
`rosbag2tools.py list topics ../src/rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3
`  

## Extract all message definitions.  
`rosbag2tools.py extract messages ../src/rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3`  

This will show you the message definitions. It useful if you want to know about a message structure so you can plot certain fields.  

## Extract topics
To write a set of topics from the rosbag2 to a text file in a human readable format use  
```
rosbag2tools.py extract topics -n /current_pose,/current_velocity ../src/rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3
```  

A new text file named topics.txt will be created in a sub dir under **../src/rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0/** containing just these topics deserialised.  


## Plot topics  
To plot some topic fields e.g to plot two values from different topics seperate with a comma.  
`rosbag2tools.py plot -y /current_pose.pose.position.y,/pacmod/as_rx/steer_cmd.command ./rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3`  

To Plot a topic value against another topic value use  
`rosbag2tools.py plot -x /current_pose.pose.position.x -y /current_pose.pose.position.y ./rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3`  

Plotting is done using the python plotly library and firefox. A new firefox tab should automatically open after you run the above plot commands.    

The flag -x when not specified is automatically populated with the value index. This causes any plots to be plotted against an incrementing index number. 

There are some special fields such as header.stamp which is automatically converted to seconds to plot against. E.g  
`rosbag2tools.py plot -x /current_pose.header.stamp -y /current_velocity.twist.linear.x ./rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3`  
This will plot the x velocity against the time stamp from the message headers.

More complicated examples  
`rosbag2tools.py plot -x /current_pose.pose.position.x,index,index,/current_velocity.header.stamp -y /current_pose.pose.position.y,/pacmod/as_rx/steer_cmd.command,/pacmod/as_rx/accel_cmd.command,/current_velocity.twist.linear.x ../src/rosbag2_2020_01_13-12_36_14/rosbag2_2020_01_13-12_36_14_0.db3`  

An example of a plot made from a command similar to the ones shown looks like this.
![example plot](./pics/Screenshot from 2020-02-11 13-34-36.png)


# To Rebuild the docker container 
`docker build -f docker_files/Dockerfile -t rosbag2_tools:latest .`

